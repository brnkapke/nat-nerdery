#Nerdery FED NAT
*** 
##View the project [here](http://yourshoesuntied.com/nat).
  
##Overview
To the conditions and objectives in the instructions have been met to the best of my ability.  While there are some things that could be improved,
The overall code base should is stable, modular and easy to extend or integrate with a CMS.  It was a fun comp to work with and had a lot of nice little
design challenges.

See the project requirements and objectives [below](#markdown-header-instructions-and-objective)  

#Documentation
***

## Carousel implementation
A simplified demo file, [carousel-demo.html](http://yourshoesuntied.com/nat/carousel-demo.html) is included in the source
###Dependencies 
jQuery

###Example initialization

    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="lib/carousel-js/carousel.js"></script>
    <link type="text/css" rel="stylesheet" href="css/carousel-base.css">
        
        
    <script>
        $(document).ready(function (){
            var settings = {
                interval : 4500,
                pauseForInteraction : true,
                cssOverride : false,
                crossfade : true
            };
            var carousel = new Carousel($("#carousel"), settings);
        });
    </script>
    
###Example HTML

    <div id="carousel" class="carousel">
        <ul class="slides">
            <li class="slide active-slide" id="slide1">
                <img src="img/carousel/baltimore.jpg" alt="Downtown Baltimore">
            </li>
            <li class="slide" id="slide2">
                <img src="img/carousel/venice.jpg" alt="Exploring the Venetian">
            </li>
            <li class="slide" id="slide3">
                <img src="img/carousel/london.jpg" alt="London After Dark">
            </li>
            <li class="slide" id="slide4">
                <img src="img/carousel/mount-rushmore.jpg" alt="Mount Rushmore">
            </li>
        </ul>

        <ul class="carousel-nav">
            <li><a href="#slide1" class="control active-control"><img src="img/carousel/thumbs/baltimore.jpg" alt="Downtown Baltimore slide control"><div class="carousel-thumb-frame"></div></a></li>
            <li><a href="#slide2" class="control"><img src="img/carousel/thumbs/venice.jpg" alt="Exploring the Venetian slide control"><div class="carousel-thumb-frame"></div></a></li>
            <li><a href="#slide3" class="control"><img src="img/carousel/thumbs/london.jpg" alt="London After Dark slide control"><div class="carousel-thumb-frame"></div></a></li>
            <li class="last-slide-control"><a href="#slide4" class="control"><img src="img/carousel/thumbs/mount-rushmore.jpg" alt="Mount Rushmore slide control"><div class="carousel-thumb-frame"></div></a></li>
        </ul>
    </div>

###Validation
CSS validates [here.](http://jigsaw.w3.org/css-validator/validator?uri=yourshoesuntied.com%2Fnat&profile=css3&usermedium=all&warning=1&vextwarning=&lang=en)  

HTML5 validates [here](http://validator.w3.org/check?uri=http%3A%2F%2Fyourshoesuntied.com%2Fnat%2F&charset=%28detect+automatically%29&doctype=Inline&group=0&user-agent=W3C_Validator%2F1.3+http%3A%2F%2Fvalidator.w3.org%2Fservices)  
The one exception is caused by the height setting in the twitter widget.  I opted to leave the errror rather than mess with way the widget is called.

Accessibility compliance tested [here](http://achecker.ca/checker).  
Only one issue with the white text on blue background in the RECENT ARTICLES heading.  

##Known issues and variation.
###Navigation
In the comp, the navigation tabs appeared to be sized to the content while dividing the space available.  For the purposes of a dynamic site
I chose to equally divide the area into 6 equal portions.  This seemed to be a more stable cross browser solution that avoidied using tables
or dom manipulation to control the tab links.  It also seemed more practical in terms of using dynamic content.

###Twitter Widget
The twitter widget does not validate because of the way that a height is applied to it.
    <a ... height="353">Tweets by @CrispinMulberry</a>
Also, in the comp, the follow button includes the "@CrispinMulberry" on the burtton.  In attempted a few fixes and tries at fixing/applying this
look to the api.  I was not able to determine whether or not the bug was a twiiter bug, or a change in their api since the comp was generated.

###Newsletter sign up
The comp included a "Contests & Promotions" checkbox that was obscured.  Returning it to the design disturbs the sidebar, but may actaully simulate
additional categories being added to the newsletter.  Though removing it would be an easy change, I opted to include it in the web template since it seemed important to consider that particular checkbox, 
as well as any additional checkboxes required in the future. 

###Input placeholders
In my testing I noted that IE 9 and earlier does not support the placeholder attribute.  After considering a variety of solutions, I opted to, for ie 9 and earlier, set
the input cell's value attribute to be the same as the placeholder.  When the field is in focus, the place holder is value is cleared.  This may not be the most elegant solution but if necessary,
some js could be written to better add placeholder support.  There are existing libraries that could be implemented as well.  Analytics, design, and overall functionality should
be considered before more time is spent.

###Background image
I went back and forth betweem allowing the image to resize and fixing its width.  Overall I think a fixed width works better and stays true to the
design.  As with anything this is subjective and has some room for improvements.

##Potential Improvments and Enhancements

###Responsive

Consideration to responsive design was made when structuring the code but no responsive code was written.  If data requires supporting more screens,
the template could be adapted and made responsive.  This would require a design and break oints to be determined first.

###Carousel

The carousel could be further developed.  Potential new features could include next, last, beginning and end controls.  Multiple annimations and
transitions.  A generic basic stylesheet needs to be created as well.  The styles are currently part of the greater code base.

###Sass or Less Integration

I avoided using a preprocessor in an effort to show what I know and not what the big brains over at Sass, Less, and Compass know.  However, it may be worth 
considering as it can help with development speed, browser compliance and general organization of the style code.

##THANKS FOR READING!

## Instructions and Objective  

You have been provided a PSD with a design of an interior page of a website. Please take this asset and turn it into a fully
functional static page that could be used for integration with a yet to be determined CMS. This will be a much larger site
than a single page, and your code should be written to be flexible and reusable throughout the site. Show us your best start
to a code base that will be scalable, flexible, maintainable, semantic and accessible. Build in a progressively enhanced manner -
attention to detail should be high, but minor aesthetic details may differ depending on the native functionality of each required
browser. As you make these decisions, justify them in a read me file.

##Challenge Requirements


###Browser Suport    

* IE7+
* Chrome - Latest Stable Release
* Firefox - Latest Stable Release
* Safari - Latest Stable Release


###Build Tools  


* If build tools are used, please provide the source files and a
working build that is not minified / uglified.

Frameworks, Plugins, and Polyfills  
* With the exception of jQuery, please avoid frameworks, plugins and polyfills for HTML, CSS, & JS - We'd like to see what you're capable of doing when handcrafting your own code.

###Carousel  


* The carousel should support up to a max of 4 images but there may
be less when integrated with the back end
* The carousel should auto-rotate on page load and stop rotating
when the user interacts with it, continuing its rotation once the user
is no longer in the hit area of the carousel.

###Media 

* Please use the images in the PSD and not placeholder images
* Videos do not need to play

###Twitter Widget

* Please embed a standard Twitter widget for the user @crispinmulberry and include a working copy in the code.

###Ads

* Use static images for the ads, no integration is necessary.

###Forms

* Forms do not need to submit, but should be marked up
appropriately to be a functional form

###Pixel Perfection  

* Make sure the design matches the PSD in all required browsers






