/*jshint devel:true*/
var Carousel = function(elem, carouselSettings) {
    var self = this;
    
    /* TODO: refactor and accept the settings when the carousel is declared.
     * Make sure default values are present if settings have not been declared
     */
    
    var settings = {};
    
    /* set the settings, or the defaults */
    settings.interval = !carouselSettings.interval ? 4500 : carouselSettings.interval;
    settings.pauseForInteraction = !carouselSettings.pauseForInteraction ? true : carouselSettings.pauseForInteraction;
    settings.cssOverride = !carouselSettings.cssOverride ? false : carouselSettings.cssOverride;
    settings.crossfade = !carouselSettings.crossfade ? true : carouselSettings.crossfade;
    
    
      
    
    //Set and declare appropriate vars
    var carouselId = '#' + elem.attr('id'),
        activeSlide = null,
        activeControl = $('.active-control'),
        activeSlideId,
        slides = [],
        controls = [];
    
    //Loop to get the thumbnail control tabs from the dom
    $.each($(carouselId + ' .control'), function(id, controlTab) {
        var control = {
            element : controlTab,
            id : $(controlTab).attr('href'),
            index: id
        };
        controls.push(control);
    });
    
    //------------ Loop to get the thumbnail slides and store them --------------//
    $.each($(carouselId + ' .slide'), function(id, slideItem){
        var slide = {
            element : slideItem,
            id : $(slideItem).attr('id'),
            controlTabElement : $.grep(controls, function(control){
                if ('#' + $(slideItem).attr('id') === control.id) {
                    return control;
                }
            }),
            index: id
        };
        
        //Set the activeSlide
        if ($(slide.element).hasClass('active-slide')) {
            activeSlide = slide;
        }
        
        slides.push(slide);
    });


    //---- updates the active control.  Must pass in the next slide object ----------//
    function updateControls(nextSlideObject) {
        $(activeControl).toggleClass('active-control');
        var nextTab = nextSlideObject.controlTabElement[0];
        $(nextTab.element).toggleClass('active-control');
        activeControl = $(nextTab.element);
    }
    
    //------------Function to swap a visible element with a new element -------------//
    
    function transitionElements(elementOut, elementIn) {
        //Use CSS CLASSES to handle transition

        //The following will
        if (settings.cssOverride) {
            elementOut.toggleClass('active-slide');
            elementIn.toggleClass('active-slide');
        }

        
        //USE JQUERY to animate the change
        if (settings.crossfade) {
            elementIn.fadeIn(250);
            elementOut.fadeOut(350, function() {
                
                elementOut.toggleClass('active-slide');
                elementIn.toggleClass('active-slide');
            });
        }

    }
    
    //------------ pass in the slide object to show and this fn will update the view -------------//
    
    function updateView(nextSlideObject) {

        var nextSlide = $(nextSlideObject.element),
            currentSlide = $(activeSlide.element);
        
        transitionElements(currentSlide, nextSlide);
        
        //track Slides
        activeSlide = nextSlideObject;
        activeSlideId = '#' + activeSlide.id;
        
        //update the controller States
        updateControls(nextSlideObject);
        
    }
    
    
    //------------ This function allows us to ask for the slide by it's html id -------------//
    
    function getSlideByHtmlId(newSlideId) {
        
        if (activeSlideId === newSlideId) {
            return false;
        } else {
            var nextSlideObject = $.grep(slides, function( slide ) {
                if(('#' + slide.id) === newSlideId) {
                    return slide;
                }
            });
            //TODO: this could be less muddy.  Maybe refine the objects to be more specific.
            updateView(nextSlideObject[0]);
        }
        return false;
    }
    
    //---------- Listen up! Listen for clicks on the filmstrip/tabs/thumbs and handle the event -------//
    $('.control').click(function (e){
        if (slides.length > 1) {
            var targetElement = $(this),
            newSlideID = targetElement.attr('href');
            getSlideByHtmlId(newSlideID);
        }
        return false;
    });
    
    
    // --------- handle interaction requirements pause and play based on users focus -------------//
    
    if (settings.pauseForInteraction) {
        //On mouseover pause the carousel
        $('.carousel').bind('mouseover', function() {
            if (self.autoPlay) {
                self.autoPlay = false;
            }
        });
        
        //On mouseoout pause the carousel
        $('.carousel').bind('mouseout', function() {
            if (!self.autoPlay) {
                self.autoPlay = true;
            }
        });
    }
    
    // ------ cycle through the images automatically ---------//
    function rotate() {
        //Check for the index;
        var slideCount = slides.length,
            nextSlideObject;
            
        if (activeSlide.index < (slideCount -1)) {
            nextSlideObject = slides[activeSlide.index + 1];
        } else {
            nextSlideObject = slides[0];
        }
        
        updateView(nextSlideObject);
        //changeSlides(nextSlideObject.id);
    }
    
    
    // -------- cycles through the slide array based on the interval ------//
    setInterval(function(){
        if (self.autoPlay && slides.length > 1) {
            rotate();
        }

    }, settings.interval);

    //-------------  methods These may be called in the case that buttons or controls are needed ------------------//
    
    this.autoPlay = true;
    
    this.pause = function() {
        self.autoplay = false;
    };
    
    this.play = function() {
        self.autoplay = false;
    };
    
    this.next = function() {
        //get the slide index and show the next slide. 
    };
    
    this.prev = function() {
        //get the slide index and show the previous slide.
    };

    
};