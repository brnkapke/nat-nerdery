/*jshint devel:true */
/*global Carousel */
/*global $, jQuery*/
$(document).ready(function () {
    "use strict";
    
    var carouselSettings = {
        interval : 4500,
        pauseForInteraction : true,
        cssOverride : false,
        crossfade : true
    };
    
    this.carousel = new Carousel($("#carousel"), carouselSettings);
    /*
     *Add site js in this file
    */
});